<?php
ini_set('display_errors', 'On');
if ($_REQUEST) {
    $sum = 0;
    foreach($_REQUEST as $item){
        if(is_numeric($item)){
            $sum = $sum + $item;
        }
    }
    $avg = $sum/10;
    // echo "میانگین نمره شما " . $avg . "میباشد" ;
    // echo "<br>";
    // if($avg>12){
    //     echo "شما پاس شدید";
    // }else{
    //     echo "متاسفانه شما افتادید";
    // }
    echo '
        <head>
            <meta charset="UTF-8">
            <link rel="icon" href="../image/favicon.ico" />
            <meta name="description" content="home page">
            </meta>
            <title> mobina saffary Report Card </title>
            <h2> کارنامه مبینا صفاری </h2>
            <style>

                td {
                    border: 1px solid #726E6D;
                    padding: 15px;
                  }

                  thead{
                    font-weight:bold;
                    text-align:center;
                    background: #625D5D;
                    color:white;
                  }

                  table {
                    border-collapse: collapse;
                  }

                  .footer {
                    text-align:right;
                    font-weight:bold;
                  }

                  tbody >tr:nth-child(odd) {
                    background: #D1D0CE;
                  }
            </style>
        </head>
        <body>
          <table>
            <thead>
              <tr>
                <td>دروس </td>
                <td>نمرات </td>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td>'. $_REQUEST["name1"] .'</td>
                <td>'. $_REQUEST["num1"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name2"] .'</td>
                <td>'. $_REQUEST["num2"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name3"] .'</td>
                <td>'. $_REQUEST["num3"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name4"] .'</td>
                <td>'. $_REQUEST["num4"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name5"] .'</td>
                <td>'. $_REQUEST["num5"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name6"] .'</td>
                <td>'. $_REQUEST["num6"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name7"] .'</td>
                <td>'. $_REQUEST["num7"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name8"] .'</td>
                <td>'. $_REQUEST["num8"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name9"] .'</td>
                <td>'. $_REQUEST["num9"] .'</td>
              </tr>
              <tr>
                <td>'. $_REQUEST["name10"] .'</td>
                <td>'. $_REQUEST["num10"] .'</td>
              </tr>
            </tbody>
            <tfoot>
              <tr>
                <td class="footer">جمع نمرات</td>
                <td>'. $sum .'</td>
              </tr>
              <tr>
                <td class="footer">معدل کل</td>
                <td>'. $avg .'</td>
              </tr>
          </table>
        </body>';
    die();
};
header("Location: http://phplearn.local/t2/form.html");
