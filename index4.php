<?php
ini_set('display_errors', 'On');

include('./db_connect.php');

if ($_REQUEST) {

  $name = $_REQUEST["name"];
  $model = $_REQUEST["model"];
  $country = $_REQUEST["country"];
  $type = intval($_REQUEST["type"]);

  $q = "INSERT INTO cars (name, model, country ,type) 
          VALUES ('$name' , '$model' , '$country' , $type);";
  $q_result = mysqli_query($conn, $q);

  $msg = "";
  if ($q_result == true) {
    $msg = "ماشین مورد نظر ذخیره شد";
  } else {
    $msg = "ماشین مورد نظر ذخیره نشد ... دوباره تلاش کنید";
  };
  echo '<script>
      alert("' . $msg . '");
    </script>';
};

$query = "SELECT * FROM cars";
$query_result = mysqli_query($conn, $query);
$count = mysqli_num_rows($query_result);
$info = mysqli_fetch_all($query_result);

echo '
  <head>
      <meta charset="UTF-8">
      <link rel="icon" href="../image/favicon.ico" />
      <meta name="description" content="home page">
      </meta>
      <title> mobina saffary Report Card </title>
      <h2 style="text-align: center;"> ماشین های ثبت شده تا الان </h2>
      <style>
          td {
            border: 1px solid #726E6D;
            padding: 15px;
          }
          thead{
            font-weight:bold;
            text-align:center;
            background: #625D5D;
            color:white;
          }
          table {
            border-collapse: collapse;
          }
          tbody >tr:nth-child(odd) {
            background: #D1D0CE;
          }
          .center {
            margin: auto;
            width: 27%;
            padding: 10px;
          }
          .button {
            background-color: #D1D0CE;
            margin: auto;
            width: 90%;
            padding: 10px;
            border: 1px solid black;
            border-radius: 25px;
          }
      </style>
  </head>
  <body>
  <div class="center">
    <table>
      <thead>
        <tr>
          <td>شماره شناسه </td>
          <td>نام </td>
          <td>مدل </td>
          <td>کشور سازنده </td>
          <td>نوع </td>
        </tr>
      </thead>
      <tbody>';

foreach ($info as $value) {
  echo '<tr>
          <td>' . $value["0"] . '</td>
          <td>' . $value["1"] . '</td>
          <td>' . $value["2"] . '</td>
          <td>' . $value["3"] . '</td>
          <td>' . $value["4"] . '</td>
        </tr>';
};

echo '</tbody>
    </table>
    <br>
    <button type="button" class="button">
      <a href="./t4/form.html"> ایجاد یک ماشین دیگر</a>
    </button>
  </div>
  </body>';
