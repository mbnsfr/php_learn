<?php
ini_set('display_errors', 'On');

include('./db_connect.php');

if ($_REQUEST) {

  $title = $_REQUEST["title"];
  $text = $_REQUEST["text"];
  $type = intval($_REQUEST["type"]);
  $img = $_REQUEST["img"];

  $q = "INSERT INTO news (news_title, news_text, news_type,news_image) 
          VALUES ('$title' , '$text' , '$type' , '$img');";
  $q_result = mysqli_query($conn, $q);

  $msg = "";
  if ($q_result == true) {
    $msg = "خبر مورد نظر ذخیره شد";
  } else {
    $msg = "خبر مورد نظر ذخیره نشد ... دوباره تلاش کنید";
  };
  echo '<script>
      alert("' . $msg . '");
    </script>';
};

$query = "SELECT * FROM news";
$query_result = mysqli_query($conn, $query);
$count = mysqli_num_rows($query_result);
$info = mysqli_fetch_all($query_result);

echo '
  <head>
      <meta charset="UTF-8">
      <link rel="icon" href="../image/favicon.ico" />
      <meta name="description" content="home page">
      </meta>
      <title>mobina Saffary midterm exam </title>
      <h2 style="text-align: center;">
      خبر های ثبت شده تا الان 
        <br/> 
      </h2>
      <style>
          td {
            border: 1px solid #726E6D;
            padding: 15px;
          }
          thead{
            font-weight:bold;
            text-align:center;
            background: #625D5D;
            color:white;
          }
          table {
            border-collapse: collapse;
          }
          tbody >tr:nth-child(odd) {
            background: #D1D0CE;
          }
          .center {
            margin: auto;
            width: 27%;
            padding: 10px;
          }
          .button {
            background-color: #D1D0CE;
            margin: auto;
            width: 90%;
            padding: 10px;
            border: 1px solid black;
            border-radius: 25px;
          }
          .responsive {
            width: 100%;
            height: auto;
          }
      </style>
  </head>
  <body>
  <div class="center">
    <table>
      <thead>
        <tr>
          <td>شناسه </td>
          <td>عنوان خبر</td>
          <td>متن خبر</td>
          <td>نوع خبر</td>
          <td>عکس خبر</td>
        </tr>
      </thead>
      <tbody>';

foreach ($info as $value) {
  $news_type = "";
  if ($value["3"] == 1) {
    $news_type = "سیاسی";
  } elseif ($value["3"] == 2) {
    $news_type = "علمی";
  } elseif ($value["3"] == 3) {
    $news_type = "ورزشی";
  }
  echo '<tr>
          <td>' . $value["0"] . '</td>
          <td>' . $value["1"] . '</td>
          <td>' . $value["2"] . '</td>
          <td>' . $news_type . '</td>
          <td><img src="/image/' . $value["4"] . '" alt="flowerImage" class="responsive"></td>
        </tr>';
};
echo '</tbody>
    </table>
    <br>
    <button type="button" class="button">
      <a href="./form.html"> ایجاد یک خبر دیگر</a>
    </button>
  </div>
  </body>';
